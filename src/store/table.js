/* eslint-disable no-unused-vars */
import create from 'zustand';

export const useTableStore = create((set, get) => ({
  tableData: {
    columns: [
      {
        field: 'id',
        headerName: 'ID',
        width: 100,
      },
      {
        field: 'fullName',
        headerName: 'Full Name',
        width: 300,
      },
      {
        field: 'email',
        headerName: 'Email',
        width: 200,
      },
      {
        field: 'phoneNumber',
        headerName: 'Phone Number',
        type: 'number',
        width: 200,
      },
      {
        field: 'gender',
        headerName: 'Gender',
        type: 'number',
        width: 200,
      },
    ],
    rows: [
      {
        id: 1,
        fullName: 'Srikrishna Elangovan',
        email: 'srikrishna123@gmail.com',
        phoneNumber: '+913 98745 63210',
        gender: 'Male',
      },
      {
        id: 2,
        fullName: 'Srikrishna Elangovan',
        email: 'srikrishna123@gmail.com',
        phoneNumber: '+913 98745 63210',
        gender: 'Male',
      },
      {
        id: 3,
        fullName: 'Srikrishna Elangovan',
        email: 'srikrishna123@gmail.com',
        phoneNumber: '+913 98745 63210',
        gender: 'Male',
      },
      {
        id: 4,
        fullName: 'Srikrishna Elangovan',
        email: 'srikrishna123@gmail.com',
        phoneNumber: '+913 98745 63210',
        gender: 'Male',
      },
      {
        id: 5,
        fullName: 'Srikrishna Elangovan',
        email: 'srikrishna123@gmail.com',
        phoneNumber: '+913 98745 63210',
        gender: 'Male',
      },
      {
        id: 6,
        fullName: 'Srikrishna Elangovan',
        email: 'srikrishna123@gmail.com',
        phoneNumber: '+913 98745 63210',
        gender: 'Male',
      },
      {
        id: 7,
        fullName: 'Srikrishna Elangovan',
        email: 'srikrishna123@gmail.com',
        phoneNumber: '+913 98745 63210',
        gender: 'Male',
      },
      {
        id: 8,
        fullName: 'Srikrishna Elangovan',
        email: 'srikrishna123@gmail.com',
        phoneNumber: '+913 98745 63210',
        gender: 'Male',
      },
      {
        id: 9,
        fullName: 'Srikrishna Elangovan',
        email: 'srikrishna123@gmail.com',
        phoneNumber: '+913 98745 63210',
        gender: 'Male',
      },
      {
        id: 10,
        fullName: 'Srikrishna Elangovan',
        email: 'srikrishna123@gmail.com',
        phoneNumber: '+913 98745 63210',
        gender: 'Male',
      },
      {
        id: 11,
        fullName: 'Srikrishna Elangovan',
        email: 'srikrishna123@gmail.com',
        phoneNumber: '+913 98745 63210',
        gender: 'Male',
      },
      {
        id: 12,
        fullName: 'Srikrishna Elangovan',
        email: 'srikrishna123@gmail.com',
        phoneNumber: '+913 98745 63210',
        gender: 'Male',
      },
    ],
  },
  editJson: null,
  handleJsonSave: () => {
    const { editJson } = get();
    set({
      tableData: JSON.parse(editJson),
    });
  },
  handleChange: (editJson) => {
    set({
      editJson,
    });
  },
  handleUpdate: () => {
    const { tableData } = get();
    set({
      editJson: JSON.stringify(tableData, null, 2),
    });
  },
}));
