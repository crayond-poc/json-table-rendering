import React from 'react';
import { createBrowserRouter, RouterProvider } from 'react-router-dom';

import { TableWithNavBar, ErrorBoundary } from '@pages';

const router = createBrowserRouter([
  {
    path: '/',
    errorElement: <ErrorBoundary />,
    children: [
      {
        index: true,
        element: <TableWithNavBar />,
      },
    ],
  },
]);

function RouterApp() {
  return <RouterProvider router={router} />;
}

export default RouterApp;
