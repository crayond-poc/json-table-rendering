import { Box } from '@mui/material';
import { DataGrid } from '@mui/x-data-grid';

export function Table(props) {
  const {
    rows = [],
    columns = [],
    pageSize = 10,
    onRowClick = {},
    onRowDoubleClick = {},
    autoHeight = true,
    disableSelectionOnClick = true,
    className = '',
    ...rest
  } = props;
  return (
    <Box className={`${className}`} {...rest}>
      <DataGrid
        sx={{
          '& .MuiDataGrid-row': {
            border: '1px solid',
            borderColor: 'grey.400',
          },
          '& .MuiDataGrid-main': {
            border: '1px solid',
            borderColor: 'grey.400',
            bgcolor: '#FFFFFF',
          },
        }}
        rows={rows}
        columns={columns}
        pageSize={pageSize}
        onRowClick={onRowClick}
        onRowDoubleClick={onRowDoubleClick}
        autoHeight={autoHeight}
        disableSelectionOnClick={disableSelectionOnClick}
      />
    </Box>
  );
}
