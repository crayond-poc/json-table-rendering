import { Table as MUITable } from '@atoms';
import { Input } from '@atoms/input';
import { withNavBar } from '@lib/hoc';
import { Box, Button, Grid } from '@mui/material';
import { useTableStore } from '@store';
import React from 'react';
import { table_style } from './style';

function Table() {
  const { tableData, handleJsonSave, handleUpdate, editJson, handleChange } =
    useTableStore((state) => ({
      tableData: state.tableData,
      editJson: state.editJson,
      handleChange: state.handleChange,
      handleUpdate: state.handleUpdate,
      handleJsonSave: state.handleJsonSave,
    }));

  React.useEffect(() => {
    handleUpdate();
  }, []);

  return (
    <Box sx={table_style.rootSx}>
      <Grid container spacing={4}>
        <Grid item xs={12}>
          <Box>
            <MUITable {...tableData} />
          </Box>
        </Grid>
        <Grid item xs={12}>
          <Input
            placeholder="Please enter your form json"
            multiline
            fullWidth
            rows={20}
            sx={{
              bgcolor: 'white',
              '.MuiInputBase-input': { fontSize: '14px' },
              borderRadius: '0px',
            }}
            onChange={(e) => handleChange(e.target.value)}
            value={editJson}
          />
          <Button
            sx={table_style.saveButtonSx}
            variant="outlined"
            disableGutter
            size="small"
            onClick={() => handleJsonSave()}
          >
            Save
          </Button>
        </Grid>
      </Grid>
    </Box>
  );
}

export const TableWithNavBar = withNavBar(Table);
